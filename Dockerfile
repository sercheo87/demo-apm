FROM node:16.14.2
WORKDIR /usr/src/app
COPY package.json .
RUN yarn
RUN mkdir tmp
RUN chmod -R 777 tmp
COPY . .
RUN yarn run build
EXPOSE 3000
CMD ["yarn", "run", "start:prod"]
